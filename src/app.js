const express = require('express');
const app = express();

app.listen(9000, function () {
    console.log('%s >> %d: %s', __filename, __line, 'Connect job-queue successful');
});
