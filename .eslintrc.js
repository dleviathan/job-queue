module.exports = {
    'env': {
        'browser': true,
        'es6': true,
    },
    'extends': 'google',
    'globals': {
        'Atomics': 'readonly',
        'SharedArrayBuffer': 'readonly',
    },
    'parserOptions': {
        'ecmaVersion': 2018,
        'sourceType': 'module',
    },
    'rules': {
        'max-len': ["error", {"code": 160}],
        'indent': ["warn", 4],
        'comma-dangle': ["warn", {
            "arrays": "never",
            "objects": "never",
            "imports": "never",
            "exports": "never",
            "functions": "ignore"
        }],
        "prefer-const": ["warn", {
            "destructuring": "any",
            "ignoreReadBeforeAssign": false
        }],
        'new-cap': ["error", {"capIsNew": false, "newIsCap": false}],
        'quotes': ['off'],
        "space-before-function-paren": ["error", {
            "anonymous": "always",
            "named": "never",
            "asyncArrow": "always"
        }],
        'no-unused-vars': "warn",
    },
};
